.PHONY: all venv pypi ci.pypi test doc
INTERACTIVE:=$(shell [ -t 0 ] && echo 1)

PACKAGE_NAME:=$(shell python3 -c 'import setup; print(setup.PACKAGE_NAME)')
MODULE_NAME:=$(shell python3 -c 'import setup; print(setup.MODULE_NAME)')
DIST:=$(shell python3 -m setup)
IDENTITY:=$(shell git config user.signingkey)

PYT_ORD:=--new-first --failed-first
PYT_PDB:=--pdb --pdbcls=IPython.core.debugger:Pdb
PYT_LOG:=--log-level=DEBUG --show-capture=log
PYT_COV:=--cov-report term-missing

PYTEST:=py.test
PYTEST_OPTIONS:=--cov=$(MODULE_NAME)
ifdef INTERACTIVE
PYTEST_OPTIONS+=$(PYT_ORD) $(PYT_PDB) $(PYT_LOG) $(PYT_COV)
endif

TWINE:=twine upload --skip-existing

all: $(DIST)

venv: venv/activate
	@echo source $<

venv/activate: venv/$(PACKAGE_NAME)/bin/activate
	rm -f $@
	ln -s $(PACKAGE_NAME)/bin/activate $@

venv/$(PACKAGE_NAME)/bin/activate:
	rm -rf venv
	which virtualenv 2>/dev/null || pip install --user --upgrade virtualenv
	which virtualenv
	virtualenv --python=$$(which python3) venv/$(PACKAGE_NAME)
	. $@ && pip install --upgrade pip -r requirements.txt -r requirements.dev.txt -r requirements.interactive.txt

dist/%.whl:
	python3 setup.py bdist_wheel

dist/%.tar.gz:
	python3 setup.py sdist

pypi: $(DIST)
	@test -n "$(DIST)"
	@TWINE_USERNAME=neze \
		TWINE_PASSWORD="$$(pass www/pypi.org | head -n1 | tr -d '\n')" \
		$(TWINE) \
		--sign --sign-with gpg2 --identity $(IDENTITY) \
		$(DIST)

ci.pypi: $(DIST)
	@test -n "$(DIST)"
	@$(TWINE) $(DIST)

.coveragerc:
	echo "[run]\nomit=$(MODULE_NAME)/__init__.py" > $@

test: .coveragerc
	$(PYTEST) $(PYTEST_OPTIONS)

doc:
	python3 setup.py doc
